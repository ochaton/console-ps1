rockspec_format = "1.1"
package = "console.ps1"
version = "scm-1"
source = {
   url = "https://gitlab.com/ochaton/console-ps1.git"
}
description = {
   homepage = "https://gitlab.com/ochaton/console-ps1",
   license = "BSD-2"
}
dependencies = {
   "tarantool >= 1.9.0",
}
build = {
   type = "builtin",
   modules = {
      ["console.ps1"] = "console/ps1.lua"
   }
}
